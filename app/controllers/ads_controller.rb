class AdsController < ApplicationController
#before_action :set_ad, only: [:show]
def new
@ad=Ad.new

end

def create
	 @ad=Ad.new(ad_params)
	 if @ad.save
	 redirect_to ad_path(@ad.id)
   else 
    render 'new'
   end  
end

def show 
 @ad = Ad.find(params[:id])
end

def index
    @ads = Ad.all
end

private
    def ad_params
       params.require(:ad).permit(:name, :description, :price, :seller_id, :email, :img_url)
    end
end

