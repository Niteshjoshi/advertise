class Ad < ActiveRecord::Base
	validates_presence_of :name, :description, :price, :seller_id, :email, :img_url,length: {minimum: 3}
    validates_numericality_of :price, :seller_id

end
